</!DOCTYPE html>
<html>
<?=$this->load->view('includes/head',' ',TRUE)?>
<body>
  <?=$this->load->view('includes/header',' ',TRUE)?>
<!--Termina el menu -->
<?=$this->load->view('includes/menu',' ',TRUE)?>
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
          <h2 style="font-size: 30px;">Rellene las siguientes secciones para poder registrarse en esta institución</h2>
            <form method="get" >
              <h3 align="center" style="font-size: 20px;">Datos personales</h3>
                <div class="form-group">
                  <label for="inputname" style="font-size: 12px;">Nombre</label>
                  <input type="text" class="form-control" id="inputname" placeholder="Escriba su nombre" style="font-size: 12px;">
                </div>
                <div class="form-group">
                  <label for="inputApellido1" style="font-size: 12px;">Apellido paterno</label>
                  <input type="text" class="form-control" id="inputApellido1" placeholder="Apellido paterno" style="font-size: 12px;">
                </div>
                  <div class="form-group">
                  <label for="inputApellido2" style="font-size: 12px;">Apellido materno</label>
                  <input type="text" class="form-control" id="inputApellido2" placeholder="Apellido materno" style="font-size: 12px;">
                  </div>
                  <div class="form-group">
                  <label for="seleccionarcarrera" style="font-size: 12px;">Carrera elegida</label>
                  <select class="form-control" id="seleccionarcarrera">
                    <option value="0">...</option>
                    <option value="1" style="font-size: 12px;">Ingenería Mecatrónica</option>
                    <option value="2" style="font-size: 12px;">Ingenería Industrial</option>
                    <option value="3" style="font-size: 12px;">Ingenería Química</option>
                    <option value="4" style="font-size: 12px;">Ingenería Financiera</option>
                    <option value="5" style="font-size: 12px;">Ingenería en Biotecnología</option>
                    <option value="6" style="font-size: 12px;">Ingenería en Técnologias de la Información</option>
                    <option value="7" style="font-size: 12px;">Ingenería en Sistemas Automotrices</option>
                  </select>
                </div>
                <p style="font-size: 12px;">¿Adeuda materias?</p>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="adeudmat1" id="adeudmat1" value="option1">
                  <label class="form-check-label" for="inlineRadio1" style="font-size: 12px;">Si</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="adeudmat2" id="adeudmat2" value="option2">
                  <label class="form-check-label" for="inlineRadio2" style="font-size: 12px;">No</label>
                </div>
                <div class="form-group">
                  <label for="exampleFormControlSelect1" style="font-size: 12px;">Si su respuesta fue si, ¿Cúantas?</label>
                    <select class="form-control" id="exampleFormControlSelect1">
                      <option value="0">...</option>
                      <option >1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                  </select>
                </div>
                  <h3 align="center" style="font-size: 20px;">Datos de salud</h3>
                    <p style="font-size: 12px;">¿Padece alguna enfermedad?</p>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="enferm1" id="enferm1" value="option1">
                      <label class="form-check-label" for="inlineRadio1" style="font-size: 12px;">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="enferm2" id="enferm2" value="option2">
                      <label class="form-check-label" for="inlineRadio2" style="font-size: 12px;">No</label>
                    </div>
                    <div class="form-group">
                      <label for="inputname" style="font-size: 12px;">Si su respuesta fue si, ¿Cúal o cúales?</label>
                      <input type="text" class="form-control" id="inputenfermedades" placeholder="Escriba su nombre de las enfermedades que padezca" style="font-size: 12px;">
                    </div>
                    <p style="font-size: 12px;">¿Tienes alergias?</p>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="alergi1" id="alergi1" value="option1">
                        <label class="form-check-label" for="inlineRadio1" style="font-size: 12px;">Si</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="alergi2" id="alergi2" value="option2">
                        <label class="form-check-label" for="inlineRadio2" style="font-size: 12px;">No</label>
                      </div>
                      <div class="form-group">
                        <label for="inpualergia" style="font-size: 12px;">Si su respuesta fue si, ¿Cúal o cúales?</label>
                        <input type="text" class="form-control" id="inputalergia" placeholder="Escriba su nombre de las alegias que padezca" style="font-size: 12px;">
                      </div>
                  <h3 align="center" style="font-size: 20px;">Aspecto emocional</h3>
                  <p style="font-size: 12px;">¿Sufres ansiedad?</p>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="ansi1" id="ansi1" value="option1">
                        <label class="form-check-label" for="inlineRadio1" style="font-size: 12px;">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="ansi2" id="ansi2" value="option2">
                        <label class="form-check-label" for="inlineRadio2" style="font-size: 12px;">No</label>
                    </div>
                  <p style="font-size: 12px;">¿Sufres de estrés?</p>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="estres1" id="estres1" value="option1">
                      <label class="form-check-label" for="inlineRadio1" style="font-size: 12px;">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="estres2" id="estres2" value="option2">
                      <label class="form-check-label" for="inlineRadio2" style="font-size: 12px;">No</label>
                    </div>
                  <p style="font-size: 12px;">¿Sufres depresión?</p>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="depre1" id="depre1" value="option1">
                      <label class="form-check-label" for="inlineRadio1" style="font-size: 12px;">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="depre2" id="depre2" value="option2">
                      <label class="form-check-label" for="inlineRadio2" style="font-size: 12px;">No</label>
                      </div>
                  <p style="font-size: 12px;">¿Te sientes rechazado por tu familia o amigos?</p>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="rechaz1" id="rechaz1" value="option1">
                      <label class="form-check-label" for="inlineRadio1" style="font-size: 12px;">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="rechaz2" id="rechaz2" value="option2">
                      <label class="form-check-label" for="inlineRadio2" style="font-size: 12px;">No</label>
                    </div>
                  <p style="font-size: 12px;">¿Te adaptas facilmente a cambios?</p>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="cambio1" id="cambio1" value="option1">
                      <label class="form-check-label" for="inlineRadio1" style="font-size: 12px;">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="cambio2" id="cambio2" value="option2">
                      <label class="form-check-label" for="inlineRadio2" style="font-size: 12px;">No</label>
                    </div>
                    <div class="form-group">
                      <label for="inputname" style="font-size: 12px;">Alguna otra situación emocional que no haya sido considerada anteriormente</label>
                      <input type="text" class="form-control" id="situacionemo" placeholder="Escriba la alguna situación emocional extra" style="width:300 px">
                    </div>
              <h3 align="center">Subida de documentos</h3>
              <fieldset>
                <legend  style="font-size: 15px;">Los documentos deben ser legibles y claros</legend>
                <p  style="font-size: 12px;">Acta de Nacimiento</p>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFileLang" lang="es">
                  <label class="custom-file-label" for="customFileLang" style="font-size: 12px;">Seleccionar Archivo</label>
                </div>
                <p  style="font-size: 12px;">CURP Actualizada</p>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFileLang" lang="es">
                  <label class="custom-file-label" for="customFileLang" style="font-size: 12px;">Seleccionar Archivo</label>
                </div>
                <p  style="font-size: 12px;">Comprobante de domicilio</p>
                  <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFileLang" lang="es">
                  <label class="custom-file-label" for="customFileLang" style="font-size: 12px;">Seleccionar Archivo</label>
                </div>
                <p  style="font-size: 12px;">Comprobante de estudios del tercer año de secundaria</p>
                 <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFileLang" lang="es">
                  <label class="custom-file-label" for="customFileLang" style="font-size: 12px;">Seleccionar Archivo</label>
                </div>
                <p  style="font-size: 12px;">Ficha de admisión emitida por el plantel para cotejo</p>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFileLang" lang="es">
                  <label class="custom-file-label" for="customFileLang" style="font-size: 12px;">Seleccionar Archivo</label>
                </div>
                <p  style="font-size: 12px;">Formato de inscripción</p>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFileLang" lang="es">
                  <label class="custom-file-label" for="customFileLang" style="font-size: 12px;">Seleccionar Archivo</label>
                </div>
                <br>
                <p style="font-size: 12px;">Fotografía del aspirante</p>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="customFileLang" lang="es">
                  <label class="custom-file-label" for="customFileLang" style="font-size: 12px;">Seleccionar Archivo</label>
                </div>
              </fieldset>
              <div class="text-center">
                <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
                </form>
                        </div>
                      </section>
                  </section>
              	
</body>
</html>

