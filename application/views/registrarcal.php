<!DOCTYPE html>
<html>
<?=$this->load->view('includes/head',' ',TRUE)?>
<body>
<?=$this->load->view('includes/header',' ',TRUE)?>
<!--Termina el menu -->
<?=$this->load->view('includes/menu',' ',TRUE)?>
<section id="main-content">
	<section class="wrapper">
        <div class="row" >
			<h1 class="display-1">Sistema de captura de calificaciones</h1>
        </div>
    </section>
    <table class="table  table-striped" >
  <thead  class="thead-dark">
    <tr>
      <th scope="col" colspan="2" style="font-size: 20px;">Registro de calificaciones</th>
    </tr>
  </thead>
  <tbody>
  	<tr>
  		<th scope="col" colspan="2" style="font-size: 15px;">Profesor: Juan Alberto Sanchez Hernandez</th>
  	</tr>
    <tr>
      <th scope="row" style="font-size: 12px;">Nombre de la materia: Ciencias de la salud</th>
      <th scope="row" style="font-size: 12px;">No. de alumnos: 25</td>
    </tr>
    <tr>
      <th scope="row" style="font-size: 12px;">Carrera:Ingenieria Química</th>
      <th scope="row" style="font-size: 12px;">Periodo escolar: Mayo-Agosto 2020</td>
    </tr>
    <tr>
    	<th scope="row" style="font-size: 12px;">Turno: Matutino</th>
    	<th scope="row" style="font-size: 12px;">Grupo: 102</th>
    </tr>
  </tbody>
  <br>
</table>
<br>
<table class="table  table-striped">
			<thead class="thead-dark">
	<tr>
		<th style="font-size: 12px;">Matricula</th>
		<th style="font-size: 12px;">Alumno</th>
		<th style="font-size: 12px;">Parcial 1</th>
		<th style="font-size: 12px;">Parcial 2</th>
		<th style="font-size: 12px;">Parcial 3</th>
		<th style="font-size: 12px;">Parcial 4</th>
		<th style="font-size: 12px;">Asistencias</th>
		<th style="font-size: 12px;"> Calificación Final</th>
	</tr>
		</thead>
		<tbody>
			<tr>
				<th style="font-size: 12px;">1811113743</th>
				<th style="font-size: 12px;">Catalina Rodrigues Islas</th>
				<th style="font-size: 12px;"> 
					<div class="form-group row">
					  <div class="col-10">
					    <input class="form-control" type="number" value="" min="1" max="100" id="calif1">
					  </div>
					</div>
				</th>
				<th style="font-size: 12px;">
					<div class="form-group row">
					  <div class="col-10">
					    <input class="form-control" type="number" value="" min="1" max="100" id="calif2">
					  </div>
					</div>
				</th>
				<th style="font-size: 12px;">
					<div class="form-group row">
					  <div class="col-10">
					    <input class="form-control" type="number" value="" min="1" max="100" id="calif3">
					  </div>
					</div>
				</th>
				<th style="font-size: 12px;">
										<div class="form-group row">
					  <div class="col-10">
					    <input class="form-control" type="number" value="" min="1" max="100" id="calif4">
					  </div>
					</div>
				</th>
				<th style="font-size: 12px;">
				<div class="form-group row">
					  <div class="col-10">
					    <input class="form-control" type="number" value="" min="1" max="100" id="asistencia">
					  </div>
					</div>
				</th>
				<th style="font-size: 12px;">
					<div class="form-group row">
					  <div class="col-10">
					    <input class="form-control" type="number" value="" min="1" max="100" id="califinal">
					  </div>
					</div>
				</th>
			</tr>
		</tbody>
</table>
             <div class="text-center">
                <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
</section>
</body>
</html>