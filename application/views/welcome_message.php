<!DOCTYPE html>
<html lang="en">
<head>
<!--Encabezado-->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<?=$this->load->view('includes/head',' ',TRUE)?>
<!--Termina Encabezado-->
<!--Menu lateral izquierdo-->
</head>
<body>
	<!-- Inicia el menu-->
<?=$this->load->view('includes/header',' ',TRUE)?>
<!--Termina el menu -->
<?=$this->load->view('includes/menu',' ',TRUE)?>
<!--Termina menu lateral izquierdo-->
    <!--Contenido de la pagina principal-->
    <section id="main-content">
      <section class="wrapper">
        <div class="row">
            <?=$this->load->view('includes/menuprincipal',' ',TRUE)?>
        	</div>
            <div class="row" style="width: 50%; margin: auto;">
                  
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img src="assets/img/mural.jpg" alt="Mural en la Universidad" style="width:100%;">
      </div>

      <div class="item">
        <img src="assets/img/uni1.jpg" alt="Estudiantes de la Universidad" style="width:100%;">
      </div>
    
      <div class="item">
        <img src="assets/img/uni2.jpg" alt="Estudiantes de la Universidad" style="width:100%;">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
            </div>
        </div>
        </section>
    </section>
        <!--Termina en contenido de la pagina principal-->
    <!--Pie de pagina-->
<?=$this->load->view('includes/footer',' ',TRUE)?>
    <!--Termina pie de pagina-->
<?=$this->load->view('includes/base_js',' ',TRUE)?>
</body>

</html>
