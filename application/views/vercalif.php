<!DOCTYPE html>
<html>
<?=$this->load->view('includes/head',' ',TRUE)?>
<body>
<?=$this->load->view('includes/header',' ',TRUE)?>
<!--Termina el menu -->
<?=$this->load->view('includes/menu',' ',TRUE)?>
<section id="main-content">
	<section class="wrapper">
        <div class="row">
        	<h1 class="display-1">Calificaciones</h1>
        </div>
    </section>
    <table class="table  table-striped" >
  <thead  class="thead-dark">
    <tr>
      <th scope="col" colspan="2" style="font-size: 20px;">Consulta de calificaciones</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row" style="font-size: 12px;">Nombre del alumno: Catalina Rodriguez Islas</th>
      <th scope="row" style="font-size: 12px;">Matricula: 1811113743</td>
    </tr>
    <tr>
      <th scope="row" style="font-size: 12px;">Carrera:Ingenieria Química</th>
      <th scope="row" style="font-size: 12px;">Periodo escolar: Mayo-Agosto 2020</td>
    </tr>
  </tbody>
</table>
<br>
<table class="table  table-striped" >
		<thead class="thead-dark">
	<tr>
		<th style="font-size: 12px;">Materia</th>
		<th style="font-size: 12px;">Profesor</th>
		<th style="font-size: 12px;">Grupo</th>
		<th style="font-size: 12px;" scope="col" colspan="4">Parciales</th>
		<th style="font-size: 12px;" scope="col" colspan="4">Calificaciones</th>
		<th style="font-size: 12px;"> Calificación Final</th>
	</tr>
		</thead>
		<tbody>
			<tr>
				<th style="font-size: 12px;">Materia 2</th>
				<th style="font-size: 12px;"> Profesor2</th>
				<th style="font-size: 12px;">Grupo 1</th>
				<th style="font-size: 12px;">1</th>
				<th style="font-size: 12px;">2</th>
				<th style="font-size: 12px;">3</th>
				<th style="font-size: 12px;">4</th>
				<th style="font-size: 12px;">90</th>
				<th style="font-size: 12px;">80</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">92.5</th>
			</tr>
			<tr>
				<th style="font-size: 12px;">Materia 3</th>
				<th style="font-size: 12px;"> Profesor3</th>
				<th style="font-size: 12px;">Grupo 1</th>
				<th style="font-size: 12px;">1</th>
				<th style="font-size: 12px;">2</th>
				<th style="font-size: 12px;">3</th>
				<th style="font-size: 12px;">4</th>
				<th style="font-size: 12px;">90</th>
				<th style="font-size: 12px;">80</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">92.5</th>
			</tr>
			<tr>
				<th style="font-size: 12px;">Materia 4</th>
				<th style="font-size: 12px;"> Profesor4</th>
				<th style="font-size: 12px;">Grupo 1</th>
				<th style="font-size: 12px;">1</th>
				<th style="font-size: 12px;">2</th>
				<th style="font-size: 12px;">3</th>
				<th style="font-size: 12px;">4</th>
				<th style="font-size: 12px;">90</th>
				<th style="font-size: 12px;">80</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">92.5</th>
			</tr>
			<tr>
				<th style="font-size: 12px;">Materia 5</th>
				<th style="font-size: 12px;"> Profesor5</th>
				<th style="font-size: 12px;">Grupo 1</th>
				<th style="font-size: 12px;">1</th>
				<th style="font-size: 12px;">2</th>
				<th style="font-size: 12px;">3</th>
				<th style="font-size: 12px;">4</th>
				<th style="font-size: 12px;">90</th>
				<th style="font-size: 12px;">80</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">92.5</th>
			</tr>
			<tr>
				<th style="font-size: 12px;">Materia 6</th>
				<th style="font-size: 12px;"> Profesor6</th>
				<th style="font-size: 12px;">Grupo 1</th>
				<th style="font-size: 12px;">1</th>
				<th style="font-size: 12px;">2</th>
				<th style="font-size: 12px;">3</th>
				<th style="font-size: 12px;">4</th>
				<th style="font-size: 12px;">90</th>
				<th style="font-size: 12px;">80</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">92.5</th>
			</tr>
				<th style="font-size: 12px;">Materia 6</th>
				<th style="font-size: 12px;"> Profesor6</th>
				<th style="font-size: 12px;">Grupo 1</th>
				<th style="font-size: 12px;">1</th>
				<th style="font-size: 12px;">2</th>
				<th style="font-size: 12px;">3</th>
				<th style="font-size: 12px;">4</th>
				<th style="font-size: 12px;">90</th>
				<th style="font-size: 12px;">80</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">92.5</th>
			</tr>
		 <tr>
				<th style="font-size: 12px;">Materia 7</th>
				<th style="font-size: 12px;"> Profesor7</th>
				<th style="font-size: 12px;">Grupo 1</th>
				<th style="font-size: 12px;">1</th>
				<th style="font-size: 12px;">2</th>
				<th style="font-size: 12px;">3</th>
				<th style="font-size: 12px;">4</th>
				<th style="font-size: 12px;">90</th>
				<th style="font-size: 12px;">80</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">92.5</th>
		 </tr>
		 <tr>
				<th style="font-size: 12px;">Materia 8</th>
				<th style="font-size: 12px;"> Profesor8</th>
				<th style="font-size: 12px;">Grupo 1</th>
				<th style="font-size: 12px;">1</th>
				<th style="font-size: 12px;">2</th>
				<th style="font-size: 12px;">3</th>
				<th style="font-size: 12px;">4</th>
				<th style="font-size: 12px;">90</th>
				<th style="font-size: 12px;">80</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">100</th>
				<th style="font-size: 12px;">92.5</th>
		 </tr>
		</tbody>
</table>
</section>
</body>
</html>