<!DOCTYPE html>
<html>
<?=$this->load->view('includes/head',' ',TRUE)?>
<body>
<?=$this->load->view('includes/header',' ',TRUE)?>
<!--Termina el menu -->
<?=$this->load->view('includes/menu',' ',TRUE)?>
<section id="main-content">
	<section class="wrapper">
        <div class="row" >
			<h1 class="display-1">Sistema de registro de nuevos alumnos</h1>
        </div>
    </section>
    <table class="table  table-striped" >
    	<thead class="thead-dark">
    	</thead>
    	<tbody>
    <tr>
      <th scope="row" style="font-size: 12px;">Carrera: Ingenieria Química</th>
      <th scope="row" style="font-size: 12px;">Periodo escolar: Mayo-Agosto 2020</td>
    </tr>
  </tbody>
  <br>
</table>
<br>
<table class="table  table-striped">
			<thead class="thead-dark">
	<tr>
		<th style="font-size: 12px;">Folio de registro</th>
		<th style="font-size: 12px;">Nombre Completo</th>
		<th style="font-size: 12px;">¿Aceptado?</th>
	</tr>
		</thead>
		<tbody>
			<tr>
				<th style="font-size: 12px;">1811113743</th>
				<th style="font-size: 12px;">Catalina Rodrigues Islas</th>
				<th>
					<div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="rechaz1" id="acektado" value="option1">
                      <label class="form-check-label" for="inlineRadio1" style="font-size: 12px;">Si</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="radio" name="rechaz2" id="rechasado" value="option2">
                      <label class="form-check-label" for="inlineRadio2" style="font-size: 12px;">No</label>
                    </div>
				</th>
		</tbody>
    </table>
</section>
<div class="text-center">
                <button type="submit" class="btn btn-primary">Guardar</button>
              </div>
</body>
</html>