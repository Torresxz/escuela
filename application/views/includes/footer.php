</!DOCTYPE html>
<html>
<footer class="site-footer">
      <div class="text-center">
        <p>
          &copy; Copyrights <strong>Loxox</strong>. All Rights Reserved
        </p>
        <div class="credits">
          Creado en base a la platilla Doshio de <a href="https://templatemag.com/">TemplateMag</a>
        </div>
        <a href="index.html#" class="go-top">
          <i class="fa fa-angle-up"></i>
          </a>
      </div>
    </footer>
</html>