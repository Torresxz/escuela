</!DOCTYPE html>
<html>
	  <section id="container">
    <!-- **********************************************************************************************************************************************************
        Barra superior y notificaciones
        *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
      </div>
      <!--logo start-->
      <a href="http://uptlax.edu.mx/" class="logo"><b><span>Universidad</span> Politécnica de <span>Tlaxcala</span></b></a>
      <!--logo end-->
      <div class="nav notify-row" id="top_menu">
        <!--  notification start -->
        <ul class="nav top-menu">
          <!-- settings start -->
   
          <!-- settings end -->
          <!-- inbox dropdown start-->
          <!-- inbox dropdown end -->
          <!-- notification dropdown start-->
          <!-- notification dropdown end -->
        </ul>
        <!--  notification end -->
      </div>
 
    </header>
</html>